package main

import main.stm.Ref
import main.stm.stm.atomic

object Main {
    @JvmStatic
    fun main(args: Array<String>) {

        var v = Ref(10)
        atomic {
            atomic {
                var value = v.get()

                println(value)
            }
            println("Hello, world!")

        }
    }
}