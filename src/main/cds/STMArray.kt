package main.cds

import main.stm.Ref


class STMArray<T>(val size: Int) {

    private val array = Array<Ref<T>>(size, { i -> Ref<T>() })

    fun set(index: Int, value: T) {  array[index].set(value) } // /*atomic {*/ array[index].set(value) //}

    fun get(index: Int) : T = array[index].get()  ///*atomic {*/ array[index].get() //}

    fun size() = array.size
}