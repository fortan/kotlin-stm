package main.cds

import main.stm.Ref
import main.stm.stm.atomic

class STMList<T> {
    private class Node<T>(val elem: T?, prev0: Node<T>?, next0: Node<T>?) {
        val isHeader = prev0 == null
        val prev = Ref(if (isHeader) this else prev0)
        val next = Ref(if (isHeader) this else next0)
    }

    private val header = Node<T>(null, null, null)

    fun addLast(elem: T) {
        atomic {
            val p = header.prev
            val newNode = Node(elem, p.get(), header)
            p.get()?.next?.set(newNode)
            p.set(newNode)
        }
    }

    fun removeFirst(): T? {
        return atomic {
            val n = header?.next.get()
            synchronized(this) {

                if (n == header)
                    null
            }
            val nn = n?.next?.get()
            header?.next.set(nn);
            nn?.prev?.set(header)
            n?.elem
        }
    }

    fun isEmpty() = header.next == header

    override fun toString(): String {
        return atomic {
            var buf = StringBuilder("ConcurrentIntList(")
            var n = header.next.get()
            while (n != header) {
                buf.append(n?.elem?.toString())
                n = n?.next?.get()
                if (n != header) buf.append(",")
            }
            buf.append(")").toString()
        }
    }

    fun size(): Int {
        return atomic {
            var size = 0
            var n = header.next.get()
            while (n != header) {
                size++
                n = n?.next?.get()
            }
            size
        }
    }

}


