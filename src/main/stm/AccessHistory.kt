package main.stm

import java.util.*

class AccessHistory {

    private var readLogs = HashMap<Int, ReadEntry>();
    private var writeLogs = HashMap<Int, WriteEntry>();

    class WriteEntry(val ref: kotlin.jvm.internal.Ref.ObjectRef<*>, val newValue: kotlin.jvm.internal.Ref.ObjectRef<*>)

    class ReadEntry(val version: Long)

    fun updateReadLog(objectIdentifier: Int, version: Long) {
        if (!readLogs.containsKey(objectIdentifier)) {
            readLogs[objectIdentifier] = ReadEntry(version)
        }
    }

    fun updateWriteLog(objectIdentifier: Int, ref: kotlin.jvm.internal.Ref.ObjectRef<*>, newValue: kotlin.jvm.internal.Ref.ObjectRef<*>) {
        writeLogs[objectIdentifier] = WriteEntry(ref, newValue)
    }

    fun isLockedByWrite(objectIdentifier: Int): Boolean {
        return if (writeLogs[objectIdentifier] != null) true else false
    }

    fun getValue(objectIdentifier: Int): kotlin.jvm.internal.Ref.ObjectRef<*> = writeLogs[objectIdentifier]!!.newValue

    fun isReadOnly(): Boolean = writeLogs.size == 0

    fun releaseAll() {
        LocksInfo.releaseAll(writeLogs)
        writeLogs.clear()
        readLogs.clear()
    }

    fun releaseWrite() {
        LocksInfo.releaseWrite(writeLogs)
        writeLogs.clear()
    }

    fun lockAllRead() {
        LocksInfo.lockAllRead(writeLogs)
    }

    fun validate(): Boolean {
        for (a in readLogs) {
            if (LocksInfo.version(a.value.version) != LocksInfo.version(LocksInfo.getReadLock(a.key))) {
                return false
            }
        }
        return true
    }

    fun validateRead(): Boolean {
        for (a in readLogs) {
            if (LocksInfo.version(a.value.version) != LocksInfo.version(LocksInfo.getReadLock(a.key)) && !isLockedByWrite(a.key)) {
                return false
            }
        }
        return true
    }

    fun applyReadVersions(version: Long) {
        LocksInfo.updateReadVersion(readLogs, version)
    }

    fun applyWrite(version: Long) {
        LocksInfo.applyWrite(writeLogs, version)
    }

    fun print() {
        synchronized(this) {
            println("TID = " + Thread.currentThread().id)
            println("Write logs:")
            for (a in writeLogs) {
                println(a.key.toString() + " " + a.value.ref.element + " " + a.value.newValue.element)
            }

            println("Read logs:")
            for (a in readLogs) {
                println(a.key.toString() + " " + a.value.version + " " + LocksInfo.version(LocksInfo.getReadLock(a.key)))
            }
        }
    }
}


