package main.stm


class Tx(var validTs: Long, var contentionManagerTs: Long) {

    private var history = AccessHistory()

    fun isLockedByWrite(objectIdentifier: Int): Boolean = history.isLockedByWrite(objectIdentifier)

    fun updateReadLog(objectIdentifier: Int, version: Long) = history.updateReadLog(objectIdentifier, version)

    fun updateWriteLog(objectIdentifier: Int, ref: kotlin.jvm.internal.Ref.ObjectRef<*>, newValue: kotlin.jvm.internal.Ref.ObjectRef<*>) {
        history.updateWriteLog(objectIdentifier, ref, newValue)
    }

    fun validate(): Boolean = history.validate()

    fun validateRead(): Boolean = history.validateRead()

    fun getValue(objectIdentifier: Int): kotlin.jvm.internal.Ref.ObjectRef<*> {
        return history.getValue(objectIdentifier)
    }

    fun isReadOnly(): Boolean = history.isReadOnly()

    fun releaseAll() = history.releaseAll()
    fun releaseWrite() = history.releaseWrite()

    fun lockAllRead() = history.lockAllRead()

    fun updateReadVersions(version: Long) = history.applyReadVersions(version)

    fun applyWrite(version: Long) = history.applyWrite(version)

    fun print() = history.print()

}