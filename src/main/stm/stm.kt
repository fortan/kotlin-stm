package main.stm


object stm {
    fun <Z> atomic(block: () -> Z): Z = STMImpl.atomic(block)
}