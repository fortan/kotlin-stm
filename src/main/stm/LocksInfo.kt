package main.stm

import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicLong

object LocksInfo {

    private val maxMemorySize = 2.shl(20)

    private val locksRead = Array<AtomicLong>(maxMemorySize) { AtomicLong(0) }

    private val locksWrite = Array<AtomicLong>(maxMemorySize) { AtomicLong(0) }

    private val maxTID = 2.shl(20)

    private val abortFlags = Array<AtomicBoolean>(maxTID) { AtomicBoolean(false) }

    private val versionMask = 0x7FFFFFFFFFFFFFFF

    private val versionWriteMask = 0xFFFFFFFFFFF

    private val versionTIDMask = 0x7FFFF00000000000

    fun isAborted() : Boolean = abortFlags[Thread.currentThread().id.toInt()].get()

    fun setAborted(tid: Int) {
        abortFlags[tid].set(true)
    }

    fun clearAborted(tid: Int) {
        abortFlags[tid].set(false)
    }

    fun clearAborted() {
        abortFlags[Thread.currentThread().id.toInt()].set(false)
    }

    fun isLocked(lock: Long): Boolean = (lock and 1L.shl(63)) != 0L

    fun lock(lock: Long): Long = lock or 1L.shl(63)

    fun unlock(lock: Long): Long = lock and 1L.shl(63).inv()

    fun version(lock: Long): Long = lock and versionMask

    fun versionWrite(lock: Long): Long = lock and versionWriteMask

    fun getTID(lock: Long): Int = (lock and versionTIDMask).shr(44).toInt()

    fun getReadLock(objectIdentifier: Int) : Long = locksRead[objectIdentifier].get()

    fun getWriteLock(objectIdentifier: Int) : Long = locksWrite[objectIdentifier].get()

    fun setWriteLock(objectIdentifier: Int, lock: Long) {
        locksWrite[objectIdentifier].set(lock)
    }

    fun setReadLock(objectIdentifier: Int, lock: Long) {
        locksRead[objectIdentifier].set(lock)
    }

    fun releaseAll(writeLogs: HashMap<Int, AccessHistory.WriteEntry>) {
        for (log in writeLogs) {
            LocksInfo.unlockRead(log.key)
            LocksInfo.unlockWrite(log.key)
        }
    }

    fun releaseWrite(writeLogs: HashMap<Int, AccessHistory.WriteEntry>) {
        for (log in writeLogs) {
            LocksInfo.unlockWrite(log.key)
        }
    }

    fun updateReadVersion(readList: HashMap<Int, AccessHistory.ReadEntry>, version: Long) {
        for (log in readList) {
            var rLock = getReadLock(log.key)
            rLock = (rLock and versionMask.inv()) or version
            locksRead[log.key].set(rLock)
        }
    }

    fun applyWrite(writeList: HashMap<Int, AccessHistory.WriteEntry>, version: Long) {
        for (log in writeList) {
            log.value.ref.element = log.value.newValue.element
            var rLock = getReadLock(log.key)
            rLock = (rLock and versionMask.inv()) or version
            locksRead[log.key].set(rLock)
        }
    }

    fun lockAllRead(writeList: HashMap<Int, AccessHistory.WriteEntry>) {
        for (log in writeList) {
            val rLock = getReadLock(log.key)
            val newRLock = lock(rLock)
            locksRead[log.key].set(newRLock)
        }
    }

    fun tryLockWrite(objectIdentifier: Int, greedyTransactionState: Long): Boolean {
            val oldLock = getWriteLock(objectIdentifier)
            if (isLocked(oldLock)) {
                return false
            }
            val newWLock = lock(oldLock) or greedyTransactionState or (Thread.currentThread().id.shl(44))
            return locksWrite[objectIdentifier].compareAndSet(oldLock, newWLock)
    }

    private fun unlockWrite(objectIdentifier: Int) {
        val wLock = getWriteLock(objectIdentifier)
        if (isLocked(wLock)) {
            val newWLock = 0L
            locksWrite[objectIdentifier].set(newWLock)
        }
    }

    private fun unlockRead(objectIdentifier: Int) {
        val rLock = getReadLock(objectIdentifier)
        if (isLocked(rLock)) {
            val newRLock = unlock(rLock)
            locksRead[objectIdentifier].set(newRLock)
        }
    }
}