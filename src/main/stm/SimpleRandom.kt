package main.stm

/** A random number generator that focuses on speed and lack of inter-thread
 *  interference, rather than on the quality of the numbers returned.  The
 *  `object SimpleRandom` is striped internally to reduce
 *  contention when accessed from multiple threads.  The `class
 *  SimpleRandom` should only be used by a single thread.
 *  <p>
 *  The constants in this 64-bit linear congruential random number generator
 *  are from http://nuclear.llnl.gov/CNP/rng/rngman/node4.html.
 */

object SimpleRandom {
    // 64 byte cache lines are typical, so there are 8 slots per cache line.
    // This means that the probability that any two threads have false sharing is
    // p = 8 / #slots.  If there are n processors, each of which is running 1
    // thread, then the probability that no other threads have false sharing with
    // the current thread is (1-p)^(n-1).  If p is small, that is about
    // 1 - (n-1)p, which is pretty close to 1 - np.  If we want the probability
    // of false conflict for a thread to be less than k, then we need np < k, or
    // p < k/n, or 8/Slots < k/n, or #slots > 8n/k.  If we let k = 1/8, then we
    // get #slots=64*n.
    private val mask: Int
        get() {
            val min = 64 * Runtime.getRuntime().availableProcessors()
            var slots = 1
            while (slots < min) slots *= 2
            return slots - 1
        }

    private val states = LongArray(mask + 1) { it * 0x123456789abcdefL }

    /** Returns a random value chosen from a uniform distribution of all valid
     *  `Int`s.
     */
    fun nextInt(): Int {
        val id = (Thread.currentThread().id.toInt() * 13) and mask

        val next = step(states[id])
        states[id] = next

        return extract(next)
    }


    /** Returns a random value chosen from a uniform distribution of the
     *  non-negative integers less than `n`, or throws `IllegalArgumentException`
     *  if `n` is negative or zero.
     */
    fun nextInt(n: Int): Int {
        if (n <= 0)
            throw IllegalArgumentException()

        var x = -1
        while (x == -1) x = tryClamp(nextInt(), n)
        return x
    }

    private fun step(x: Long) = x * 2862933555777941757L + 3037000493L

    private fun extract(x: Long) = (x.shr(30)).toInt()

    /** r is the random, returns -1 on failure. */
    private fun tryClamp(r: Int, n: Int): Int {
        // get a positive number
        val x = r and Int.MAX_VALUE

        if ((n and -n) == n) {
            // for powers of two, we use high bits instead of low bits
            return ((x.toLong() * n).shr(31)).toInt()
        } else {
            val z = x % n
            if (x - z + (n - 1) < 0) {
                // x is bigger than floor(MAX_INT/n)*n, so we are not getting an even
                // distribution.  Try again.
                return -1
            } else {
                return z
            }
        }
    }
}