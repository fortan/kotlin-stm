package main.stm

import java.util.concurrent.atomic.AtomicLong
import kotlin.concurrent.getOrSet

object STMImpl {

    val commitTs = AtomicLong(0)
    val greedyTs = AtomicLong(0)
    var txLocal = ThreadLocal<Tx>()
    var txStackSize = ThreadLocal<Int>()
    var countSuccessfulTransactions = AtomicLong(0)
    var countRollbacks = AtomicLong(0)
    private var passLimit = 10

    init {
        txStackSize.set(0)
    }

    fun <Z> atomic(block: () -> Z): Z {
        var value: Z
        while (true) {
            if (txStackSize.getOrSet({ 0 }) > 0) {
                txStackSize.set(txStackSize.get() + 1)
                value = block()
                commit(txLocal.get())
                txStackSize.set(txStackSize.get() - 1)
            } else {
                try {
                    txLocal.set(start())
                    txStackSize.set(1)
                    value = block()
                    commit(txLocal.get())
                    txStackSize.set(txStackSize.get() - 1)
                    countSuccessfulTransactions.andIncrement
                } catch (e: RollbackError) {
                    rollback(txLocal.get())
                    txStackSize.set(0);
                    txLocal.remove()
                    countRollbacks.andIncrement
                    val max = SimpleRandom.nextInt(passLimit)
                    var count = 0
                    while (count < max) {
                        Thread.`yield`()
                        count++
                    }
                    continue
                }
            }
            break
        }
        if (txStackSize.get() == 0) {
            txLocal.remove()
        }
        return value
    }

    private fun start(): Tx {
        return Tx(commitTs.get(), greedyTs.andIncrement)
    }

    private fun commit(tx: Tx) {

        if (tx.isReadOnly()) {
            tx.releaseWrite()
            return
        }

        tx.lockAllRead()

        var ts = commitTs.incrementAndGet()

        if (ts > tx.validTs + 1 && !tx.validateRead()) {
            tx.releaseAll()
            throw RollbackError
        }

        tx.applyWrite(ts)
        tx.releaseAll()

    }

    fun getHitRatio(): Double {
        if (countRollbacks.get() + countSuccessfulTransactions.get() == 0L) {
            return 1.0
        } else {
            return countSuccessfulTransactions.get() / (countRollbacks.get() + countSuccessfulTransactions.get()).toDouble()
        }
    }

    fun getCountRollbacks() : Long {
        return countRollbacks.get()
    }

    fun getCountSuccessfulTransactions() : Long {
        return countSuccessfulTransactions.get()
    }

    private fun rollback(tx: Tx) {
        tx.releaseWrite()
    }
}