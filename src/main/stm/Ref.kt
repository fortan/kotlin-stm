package main.stm

import main.stm.LocksInfo.clearAborted
import main.stm.LocksInfo.getReadLock
import main.stm.LocksInfo.getWriteLock
import main.stm.LocksInfo.isAborted
import main.stm.LocksInfo.setAborted
import java.util.concurrent.atomic.AtomicInteger

class Ref<T>() {

    private var value = kotlin.jvm.internal.Ref.ObjectRef<T>()

    private var objectIdentifier = getNextObjectIdentifier()

    constructor(value: T) : this() {
        this.value.element = value
    }

    fun set(v: T) {
        val rLock = getReadLock(objectIdentifier)
        val wLock = getWriteLock(objectIdentifier)

        if (STMImpl.txLocal.get().isLockedByWrite(objectIdentifier)) {
            updateWriteLog(v)
            return
        }

        while (true) {
            if (LocksInfo.isLocked(wLock)) {
               if (contentionManagerShouldAbort(LocksInfo.versionWrite(wLock), LocksInfo.getTID(wLock))) {
                   throw RollbackError
               }
                continue
            }

            if (!LocksInfo.tryLockWrite(objectIdentifier, STMImpl.txLocal.get().contentionManagerTs)) {
                throw RollbackError
            }

            updateWriteLog(v)
            break
        }

        if (LocksInfo.version(rLock) > STMImpl.txLocal.get().validTs && !extend()) {
            throw RollbackError
        }
    }

    fun updateWriteLog(v: T) {
        val newValue = kotlin.jvm.internal.Ref.ObjectRef<T>()
        newValue.element = v
        STMImpl.txLocal.get().updateWriteLog(objectIdentifier, value, newValue)
    }

    fun get(): T {
        if (STMImpl.txLocal.get().isLockedByWrite(objectIdentifier)) {
            return STMImpl.txLocal.get().getValue(objectIdentifier).element as T
        }

        var version = getReadLock(objectIdentifier)
        var value: T
        while (true) {
            if (LocksInfo.isLocked(version)) {
                version = getReadLock(objectIdentifier)
                continue
            }

            value = this.value.element

            val version2 = getReadLock(objectIdentifier)

            if (version == version2) {
                break
            }

            version = version2
        }

        STMImpl.txLocal.get().updateReadLog(objectIdentifier, version)

        if (version > STMImpl.txLocal.get().validTs /* && !extend()*/) {
            throw RollbackError
        }

        return value
    }

    private fun extend() : Boolean {
        var ts = STMImpl.commitTs.get()
        if (STMImpl.txLocal.get().validate()) {
            STMImpl.txLocal.get().validTs = ts
            return true
        }
        return false
    }

    private fun contentionManagerShouldAbort(version: Long, tid: Int) : Boolean {
        if (version < STMImpl.greedyTs.get()) {
            clearAborted()
            return true
        }

        if (isAborted()) {
            clearAborted()
            return true;
        }

        setAborted(tid)
        return false
    }

    companion object {

        private val objectIdentifier = AtomicInteger(0)

        fun getNextObjectIdentifier(): Int {
            return objectIdentifier.andIncrement
        }
    }
}