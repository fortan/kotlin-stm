package test

import main.stm.STMImpl
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.*
import kotlin.concurrent.thread

class AccountTests {
    @Test
    fun noSync() {
        checkSyncMutex(10000, 100, 200, 100, Account.transfer);
    }

    @Test
    fun noSyncFast() {
        checkSyncNoMutex(100, 100, 200, 100, Account.transfer);
    }

    @Test
    fun mutexSync() {
        checkSyncMutex(100, 100, 200, 100, Account.mutexTransfer);
    }

    @Test
    fun realSTM() {
        checkSync(100, 1000, 2000, 100, AccountRef.stmTransfer);
    }

    @Test
    fun multithreadSTM() {
        checkSync(1, 1000, 2000, 1000, AccountRef.stmTransfer);
    }

    @Test
    fun oneThreadAndOneRunSTM() {
        checkSync(1, 1000, 2000, 1, AccountRef.stmTransfer);
    }

    @Test
    fun oneThreadAndMultipleRunRunSTM() {
        checkSync(100, 1000, 2000, 1, AccountRef.stmTransfer);
    }

    fun checkSync(countRun: Long, moneyAccount1: Long, moneyAccount2: Long, countThreads: Int, checkFoo: (src: AccountRef, dst: AccountRef, m: Long) -> Unit) {
        for (j in 1..countRun) {
            var acc1 = AccountRef(moneyAccount1)
            var acc2 = AccountRef(moneyAccount2)
            var pool = ArrayList<Thread>();
            for (i in 1..countThreads) {
                pool.add(thread {
                    if (i % 2 == 0) {
                        checkFoo(acc1, acc2, 1)
                        Thread.sleep(2)
                    } else {
                        checkFoo(acc2, acc1, 2)
                    }
                })
            }

            for (it in pool.iterator()) {
                it.join()
            }

            assertEquals(moneyAccount1 + moneyAccount2, acc1.balance() +
                    acc2.balance());
            assertEquals(moneyAccount1 + 2 * (countThreads / 2 + countThreads % 2) -
                    countThreads / 2, acc1.balance());
            assertEquals(moneyAccount2 - 2 * (countThreads / 2 + countThreads % 2) + countThreads / 2, acc2.balance());

        }
        println("Hit ration: " + STMImpl.getHitRatio()
                + " Successful = " + STMImpl.getCountSuccessfulTransactions()
                + " Rollbacks = " + STMImpl.getCountRollbacks())
    }

    fun checkSyncMutex(countRun: Long, moneyAccount1: Long, moneyAccount2: Long, countThreads: Int, checkFoo: (src: Account, dst: Account, m: Long) -> Unit) {
        for (j in 1..countRun) {
            var acc1 = Account(moneyAccount1)
            var acc2 = Account(moneyAccount2)
            var pool = ArrayList<Thread>();
            for (i in 1..countThreads) {
                pool.add(thread {
                    if (i % 2 == 0) {
                        checkFoo(acc1, acc2, 1)
                        Thread.sleep(2)
                    } else {
                        checkFoo(acc2, acc1, 2)
                    }
                })
            }

            for (it in pool.iterator()) {
                it.join()
            }

            assertEquals(moneyAccount1 + moneyAccount2, acc1.balance() +
                    acc2.balance());
            assertEquals(moneyAccount1 + 2 * (countThreads / 2 + countThreads % 2) -
                    countThreads / 2, acc1.balance());
            assertEquals(moneyAccount2 - 2 * (countThreads / 2 + countThreads % 2) + countThreads / 2, acc2.balance());
        }
    }

    fun checkSyncNoMutex(countRun: Long, moneyAccount1: Long, moneyAccount2: Long, countThreads: Int, checkFoo: (src: Account, dst: Account, m: Long) -> Unit) {
        for (j in 1..countRun) {
            var acc1 = Account(moneyAccount1)
            var acc2 = Account(moneyAccount2)
            var pool = ArrayList<Thread>();
            for (i in 1..countThreads) {
                pool.add(thread {
                    if (i % 2 == 0) {
                        checkFoo(acc1, acc2, 1)
                        Thread.sleep(2)
                    } else {
                        checkFoo(acc2, acc1, 2)
                    }
                })
            }

            for (it in pool.iterator()) {
                it.join()
            }
        }
    }

}