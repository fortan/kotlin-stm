package test


class Account(initialBalance: Long) {
    private var balance = initialBalance

    fun balance(): Long = balance

    fun deposit(m: Long) {
        assert(m >= 0)
        balance += m
    }

    fun thinDeposit(m: Long) {
        assert(m >= 0)
        balance += m
    }

    fun withdraw(m: Long) {
        assert(m >= 0)
        if (balance < 0)
            throw Exception()
        balance -= m
    }

    companion object {
        val transfer: (src: Account, dst: Account, m: Long) -> Unit = {
            src, dst, m ->
            src.withdraw(m)
            dst.deposit(m)
        }

        val mutexTransfer: (src: Account, dst: Account, m: Long) -> Unit = {
            src, dst, m ->
            synchronized(this) {
                src.withdraw(m)
                dst.thinDeposit(m)
            }
        }
    }
}