package test

import main.cds.STMArray
import main.stm.stm.atomic
import org.junit.Test
import java.util.*
import kotlin.concurrent.thread


class ArrayTest {
    @Test
    fun arrayPerformance() {
        arrayPerformanceOneRun()
        /*
        for (i in 0..10) {
            println("Time = " + arrayPerformanceOneRun() + " ms")
            println("Hit ration: " + STMImpl.getHitRatio()
                    + " Successful = " + STMImpl.getCountSuccessfulTransactions()
                    + " Rollbacks = " + STMImpl.getCountRollbacks())
        }*/
    }

    fun arrayPerformanceOneRun() : Long {
        val startTime = System.currentTimeMillis()


        val COUNT_THREADS = 10
        val MAX_SIZE = 10000
        val COUNT_ITER = 1000
        var accounts = STMArray<Int>(MAX_SIZE);
        for (i in 0..accounts.size - 1) {
            atomic {
                accounts.set(i, 100000)
            }
        }

        var pool = ArrayList<Thread>();
        for (i in 1..COUNT_THREADS) {
            pool.add(thread {
                for (k in 1..COUNT_ITER)
                    for (j in i * MAX_SIZE / COUNT_THREADS - 100 ..i * MAX_SIZE / COUNT_THREADS + 100) {
                        if (j < MAX_SIZE - 100) {
                            atomic {
                                var s = accounts.get(j)
                                s -= 1
                                accounts.set(j, s)
                                s = accounts.get(j)
                                s += 1
                                accounts.set(j, s)
                            }
                        }

                    }
            })
        }

        for (it in pool.iterator()) {
            it.join()
        }

        val estimatedTime = System.currentTimeMillis() - startTime
        return  estimatedTime
    }
}