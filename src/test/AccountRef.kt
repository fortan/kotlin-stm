package test

import main.stm.Ref
import main.stm.stm


class AccountRef(initialBalance: Long) {
    private var balance = Ref(initialBalance)

    fun balance(): Long = stm.atomic { balance.get() }

    fun deposit(m: Long) {
        assert(m >= 0)
        val v = balance.get()
        balance.set(v + m)
    }

    fun withdraw(m: Long) {
        assert(m >= 0)
        var v = balance.get()
        if (v < 0)
            throw Exception()
        balance.set(v - m)
    }

    companion object {
        val stmTransfer: (src: AccountRef, dst: AccountRef, m: Long) -> Unit = {
            src, dst, m ->
            stm.atomic {
                src.withdraw(m)
                dst.deposit(m)
            }
        }
    }
}


