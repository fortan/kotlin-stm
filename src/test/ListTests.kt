package test

import main.stm.STMImpl
import org.junit.Assert
import org.junit.Test
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import kotlin.concurrent.thread

class ListTests {
    @Test
    fun addToListOneThread() {
        var list = main.cds.STMList<Int>()
        for (i in 1..10) {
            list.addLast(i)
        }

        println(list.toString())

        println("Hit ration: " + STMImpl.getHitRatio()
                + " Successful = " + STMImpl.getCountSuccessfulTransactions()
                + " Rollbacks = " + STMImpl.getCountRollbacks())
    }

    @Test
    fun addToListMultipleThread() {

        for (timeRun in 1..10) {
            var counter = AtomicInteger(0)
            val COUNT_THREADS = 500
            val COUNT_OPERATIONS = 100
            var list = main.cds.STMList<Int>()
            var pool = ArrayList<Thread>();
            for (i in 1..COUNT_THREADS) {
                pool.add(thread {
                    for (j in i * COUNT_OPERATIONS..i * COUNT_OPERATIONS + COUNT_OPERATIONS) {
                        list.addLast(j)
                        if (j == i * COUNT_OPERATIONS + COUNT_OPERATIONS / 2 + 1) {
                            counter.andIncrement
                        }
                    }
                })
            }
            while (counter.get() != COUNT_THREADS);
            for (i in 1..COUNT_THREADS) {
                pool.add(thread {
                    for (j in i * COUNT_OPERATIONS..i * COUNT_OPERATIONS + COUNT_OPERATIONS / 2) {
                        list.removeFirst()
                    }
                })
            }

            for (it in pool.iterator()) {
                it.join()
            }

          //  println(list.toString())
          //  println("Hit ration: " + STMImpl.getHitRatio()
          //          + " Successful = " + STMImpl.getCountSuccessfulTransactions()
          //          + " Rollbacks = " + STMImpl.getCountRollbacks())
            Assert.assertEquals(COUNT_OPERATIONS * COUNT_THREADS / 2, list.size())
        }
    }
}