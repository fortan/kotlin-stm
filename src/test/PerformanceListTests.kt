package test;

import main.stm.STMImpl
import org.junit.Test
import java.util.*
import kotlin.concurrent.thread

public class PerformanceListTests {

    @Test
    fun addToListMultipleThread() {
        val COUNT_THREADS = 8
        val COUNT_OPERATIONS = 1000
        var list = main.cds.STMList<Int>()
        var pool = ArrayList<Thread>();
        for (i in 1..COUNT_THREADS) {
            pool.add(thread {
                for (j in i * COUNT_OPERATIONS..i * COUNT_OPERATIONS + COUNT_OPERATIONS) {
                    list.addLast(j)
                }
            })
        }
        for (i in 1..COUNT_THREADS) {
            pool.add(thread {
                for (j in i * COUNT_OPERATIONS..i * COUNT_OPERATIONS + COUNT_OPERATIONS / 2) {
                    list.removeFirst()
                }
            })
        }

        for (it in pool.iterator()) {
            it.join()
        }

        println("Hit ration: " + STMImpl.getHitRatio()
                + " Successful = " + STMImpl.getCountSuccessfulTransactions()
                + " Rollbacks = " + STMImpl.getCountRollbacks())
    }
}
